package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {
    // opprettet feltvariabler
    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random;

    // initiert konstruktør
    public Pokemon(String name) {
        this.random = new Random();
        this.name = name;
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
    }
        
    // ikke override her fordi name ikke skal endres en gang i fremtiden
    public String getName() {
        return name;
    }

    @Override // betyr at noe inni her skal endres engang. levler man opp endres strengh men name endres aldri
    public int getStrength() {
        return strength;
    }

    @Override
    public int getCurrentHP() {
        return healthPoints;
    }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;
    }

    public boolean isAlive() {
        if (healthPoints == 0) {
            return false;
        }
        return true;
    }

    @Override
    public void attack(IPokemon target) {
        // setningen nedenfor bestemmer bare hvor mange hp som skal tas fra target
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        System.out.println(this.name + " attacks " + target.getName() + "."); // this er pokemonen som angriper, target er den som blir angrepet
        target.damage(damageInflicted);
    
        if (!target.isAlive()) {
            System.out.println(target.getName() + " is defeated by " + this.name + ".");    
        }  
    }

    @Override
    public void damage(int damageTaken) {
    
        if (damageTaken >= 0) {  // Det skal ikke være mulig å gi negative skade, altså øke antall health points.
            if (((healthPoints - damageTaken) < 0)) {
                healthPoints = 0;  
            }
            else {
                healthPoints -= damageTaken; // trekker fra antall hp som damagetaken var
            }
        }
        System.out.println(name + " takes " + damageTaken + " damage and is left with " + healthPoints + "/" + maxHealthPoints + " HP");    

    }
    @Override
    public String toString() {
        return name + " HP: " + "(" + healthPoints + "/" + maxHealthPoints + ")"  + " STR: " + strength;
    }
}
